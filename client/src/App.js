import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import SearchResult from './components/searchResult';
import Modal from './components/modal';
import styled from 'react-emotion';


const HiddenDiv = styled('div')(props => ({
  display: 'none',
}));
const AppContainer = styled('div')(props => ({
  height: '100vh',
  display: 'flex',
  flexDirection: 'column',
  padding: '1em 2em',
}));
const SearchResultContainer = styled('div')(props => ({
  display: 'flex',
  flexDirection: 'column',
  width: '70em',
  textAlign: 'left',
}));
const Button = styled('button')(props => ({
  width: '20%',
  margin: '10px',
}));
const Input = styled('input')(props => ({
  width: '80%',
  margin: '0 auto',
}));
const LogoContainer = styled('div')(props => ({
  fontSize: '3em',
  paddingRight: '3%',
}));
const Letter = styled('span')(props => ({
  color: props.color
}));
const Form = styled('form')(props => ({
  display: 'flex',
  height: 'fit-content',
  textAlign: 'center',
  width: props.position === 'center' ? '35em' : '65em',
  alignItems: 'center',
  flexDirection: props.position === 'center' ? 'column' : 'row',
  margin: props.position === 'center' ? 'auto' : '0',
}));

class App extends Component {

  state = {
    searchTitle: '',
    searchResults: [],
    selectedMovie: null
  }

  updateSearchTitle = (event) => {
    this.setState({
      searchTitle: event.target.value === " " ? "-" : event.target.value
    });
  }

  searchForMovie = async (e) => {
    e.preventDefault()
    let movieTitle = (this.state.searchTitle).replace(/ /g, "-");
    let searchResults = [];
    try {
      let response = await axios.get(`http://localhost:3030/turnerchallenge/titles/titlename/${movieTitle}`); //should have an api file that this is imported from...
      if (Array.isArray(response.data)) {
        searchResults = response.data;
      } else {
        searchResults.push(response.data);
      }
      this.setState({ searchResults: searchResults });
      
    } catch (error) {
      console.log('there was an error searching for movies');
      console.log(error);
    }

  }

  selectMovie = (movie) => {
    console.log(movie);
    this.setState({ selectedMovie: movie });
  }

  closeModal = () => {
    this.setState({ selectedMovie: null });
  }

  render() {
    return (
      <AppContainer className="App">
        <Form position={this.state.searchResults.length > 0 ? 'top': 'center'} onSubmit={(e) => this.searchForMovie(e)}>
        <LogoContainer>
          <Letter color='blue'>M</Letter>
          <Letter color='red'>O</Letter>
          <Letter color='goldenrod'>O</Letter>
          <Letter color='blue'>G</Letter>
          <Letter color='green'>L</Letter>
          <Letter color='red'>E</Letter>
        </LogoContainer>
              <Input className="input is-rounded" type="text" placeholder="Type movie name here" onChange={(e) => this.updateSearchTitle(e)}/>
            <Button className="button is-outlined is-rounded" onClick={this.searchForMovie}>Search</Button>
        </Form>
        <SearchResultContainer>
          {this.state.searchResults.length > 0 ? 
            this.state.searchResults.map( (results) => (
              <SearchResult 
                key={results._id} 
                results={results} 
                selectMovie={this.selectMovie} />
            )) : <HiddenDiv />}
        </SearchResultContainer>
        {
          this.state.selectedMovie ? 
          <Modal 
            closeModal={this.closeModal} 
            movie={this.state.selectedMovie} /> :
          <HiddenDiv />
        }
      </AppContainer>
    );
  }
}

export default App;
