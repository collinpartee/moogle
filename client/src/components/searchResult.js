import React from 'react';
import styled from 'react-emotion';

const ResultContainer = styled('div')(props => ({
    padding: '10px 20px',
}));
const Source = styled('div')(props => ({
    display: '-webkit-box',
    overflow: 'hidden',
    WebkitLineClamp: '2',
    WebkitBoxOrient: 'vertical',
    maxHeight: '2.5rem',
    lineHeight: '1.2rem',
    fontSize: '1rem'
}));

const SearchResult = (props) => {
    let { results, selectMovie } = props;
    return ( 
        <div className="searchResult">
            <p className="title is-4" onClick={() => selectMovie(results)}> {results.TitleName} - {results.ReleaseYear}</p>
              <Source className="subtitle is-6">{results.Storylines[0].Description}</Source>
            </div>
     );
}
 
export default SearchResult;