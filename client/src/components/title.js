import React from 'react';
import styled from 'react-emotion';

const TitleContainer = styled('div')(props => ({
    borderBottom: '1px solid black',
    textAlign: 'left',
    fontSize: '1.3em',
}));

const Title = (props) => {
    let { title, id } = props;
    return ( 
        <TitleContainer id={id}>
            {title}
        </TitleContainer>
     );
}
 
export default Title;