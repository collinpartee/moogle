import React from 'react';

function findLongestObj (arr) {
  let _result = {};
  arr.forEach( (obj) => {
    if (Object.keys(obj).length > Object.keys(_result).length) {
      _result = obj
    }
  })
  return _result
}

function homData(data) {
  let _ideal = findLongestObj(data);
  console.log(_ideal);
  let _result;
 return Object.keys(_ideal).sort().map( (property) => {
    console.log('ideal property', property);
    _result = data.forEach( (_notIdeal) => {
      if (!_notIdeal.hasOwnProperty(property)) {
        console.log('adding ', property, 'to ', _notIdeal);
        return _notIdeal[property] = '';
      }
    })
    // return _result
  })

}

const Table = (props) => {
  let { table } = props;
  console.log(table);
  console.log( homData(table) )
  // const headerRecord = 
  return (
    <table className="table">
      <thead>
        <tr>
          {Object.keys(findLongestObj(table)).sort().map( (tableHeaders, i) => {
            return (
              <th key={i}>{tableHeaders}</th>
            )
          })}
        </tr>
      </thead>
      <tbody>
        {
          table.map( (obj, objKey) => {
            return (
              <tr key={objKey}>
                {
                  Object.keys(obj).sort().map( (data, dataKey) => {
                    if (Array.isArray(data)) {
                        data.map( (datadata, datadataKey) => {
                          return (
                            <td key={datadataKey}>{datadata}</td>
                          )
                        })
                    } else {
                      return (
                        <td key={dataKey}>{obj[data] === true ? 'yes' : obj[data] === false ? 'no' : obj[data]}</td>
                      )  
                    }
                  })
                }
              </tr>
            )
          })
        }
      </tbody>
      <tbody>
      </tbody>
    </table>
  );
}

export default Table;