import React from 'react';
import styled from 'react-emotion';

const LegendContainer = styled('div')(props => ({
    width: '20em',
    padding: '2%',
    backgroundColor: 'lightGray',
    textAlign: 'center',
}));



const Legend = (props) => {
    let { movie } = props;
    return ( 
        <LegendContainer>
            <ul>
            {
                Object.keys(movie).map( (movieKey, i) => {
                    if (movieKey === 'TitleId' || movieKey === 'TitleName' || movieKey === 'TitleNameSortable' || movieKey === '_id' || movieKey === '/') return null;
                    return (
                        <li key={i}>
                        <a href={`#${movieKey}`}>
                        {movieKey}
                        </a>
                        </li>
                    )
                })
            }
            </ul>/
        </LegendContainer>
     );
}
 
export default Legend;