import React from 'react';
import styled from 'react-emotion';
import Legend from './legend';
import Table from './table';
import Title from './title';

const ModalContainer = styled('div')(props => ({
  backgroundColor: 'white',
  position: 'absolute',
  width: '100%',
  height: '100%',
  left: '0',
  top: '0',
  padding: '1em 2em'
}));

const TopBar = styled('div')(props => ({
  textAlign: 'right',
  marginRight: '5%',
}));
const ModalTitle = styled('div')(props => ({
  textAlign: 'left',
  fontFamily: 'cursive',
  fontSize: '1.7em',
  fontWeight: '600',
  borderBottom: '1px solid',
  margin: '0 2%',
}));
const ModalBody = styled('div')(props => ({

}));
const Section = styled('div')(props => ({
  display: 'flex',
  flexDirection: 'row',
  margin: '1% 4%',
}));
const DescriptionContainer = styled('div')(props => ({
  width: '80%',
  textAlign: 'left',
  fontSize: '1.4em',
}));

const Modal = (props) => {
  let { closeModal, movie } = props;
  return (
    <ModalContainer>
      <TopBar>
        <div onClick={() => closeModal()}>close</div>
      </TopBar>
      <ModalTitle>{movie.TitleName}</ModalTitle>
      <ModalBody>
        <Section>
          <div style={{ display: 'flex', flexDirection: "column" }}>
            <DescriptionContainer>{movie.Storylines[0].Description}</DescriptionContainer>
            <Legend movie={movie} />

          </div>
          <img src="https://picsum.photos/300/?random" alt="" />
        </Section>

        {/* This should probably be its own component.. combining title and section*/}
        <Title id="Awards" title={'Awards'} />
        <Section >
          <Table table={movie.Awards} />
        </Section>
        {/* .... */}

        <Title id="Genres" title={'Genres'} />
        <Section>
          {movie.Genres.map((genre, genreId) => {
            return (<div key={genreId}>{genre}, </div>)
          })}
        </Section>

        <Title id="OtherNames"title={'Other Names'} />
        <Section>
          <Table table={movie.OtherNames} />
        </Section>

        <Title id="Participants" title={'Participants'} />
        <Section>
          {/* Need to handle sort order... */}
          <Table sortBy={'SortOrder'} table={movie.Participants} />
        </Section>

        <Title id="ReleaseYear" title={'Release Year'} />
        <Section>
        {movie.TitleName} released in {movie.ReleaseYear}.
        </Section>

        <Title id="Storylines" title={'Storylines'} />
        <Section>
          {/* Took the easy way out and made everything into a table...real life probably make a collapsible component.. */}
        <Table table={movie.Storylines} />
        </Section>
      </ModalBody>
      {/* Should probably add a back to top button.... */}
    </ModalContainer>
  );
}

export default Modal;