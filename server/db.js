const MongoClient = require('mongodb').MongoClient

var db

MongoClient.connect('mongodb://readonly:turner@ds043348.mongolab.com:43348/dev-challenge', (err, client) => {
  if (err) return console.log(err)

  db = client.db('dev-challenge').collection('Titles');

});

function getDB () {
  return db;
}

module.exports = getDB;