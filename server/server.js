const express = require('express');
const bodyParser = require('body-parser');
const api = require('./api');

const app = express();

app.use(bodyParser.urlencoded({ extended: true }))

app.get('/turnerchallenge/titles/titlename/:titlename', api)

app.listen(3030, () => {
  console.log('listening on 3030')
})