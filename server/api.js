const db = require('./db');

function titleSearch(req, res) {
    let titlename = (req.params.titlename).replace(/-/g, " ");
    let response;
    console.log(`searching for movie with title ${titlename}`);
    db().find({TitleName: new RegExp(titlename, 'i')}).toArray(function (err, result) {
        console.log(result.length);
        res.status(200).send(result);
    })
}
module.exports = titleSearch;